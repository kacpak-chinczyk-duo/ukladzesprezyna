﻿// Nie rozbijam już tego na wiele plików, więc musisz sam ogarnąć co gdzie powinno pójść
// Makefile nigdy też nie robiłem, bo nie potrzebowałem, więc musisz wygooglować

#include <stdio.h>
#include <stdlib.h>

// Dałem tutaj mase i predkosc, mozesz dodac wspolczynniki polozenia (x, y) np, co tam potrzebujesz,
//  a potem w zmiennej je zaincjalizowac
//  nie musisz inicjalizowac wszystkich zmiennych struktury, ale zaleznie od kompilatora bede 0, NULL lub co tam akurat bylo w pamieci wiec randomowa liczba
typedef struct {
    double masa;            // Masa w kg
    double predkosc;        // Prędkość w m/s
} MasaPunktowa;

typedef struct {
    double stala;           // Stała sprężystości [k] w  N/m
    double dlugosc;         // Długość [l] w m
    MasaPunktowa *m1;       // Wskaźniki na masy punktowe na początku
    MasaPunktowa *m2;       //  i na końcu sprężyny
} Sprezyna;

double silaSprezyny(const Sprezyna *s) {        // domyślnie argument to był 'const Sprezyna &s' ale mój kompilator wariował, więc weź to pod uwagę że może nie być kompatybilne z Twoim
    return 0.0; // wzór na siłę sprężyny
}

double energia(const Sprezyna *s) {             // problem jak wyżej
    return 0.0;
}

double energia(const MasaPunktowa *m) {         // problem jak wyżej
    return 0.0;
}

int main() {
    MasaPunktowa m1 = { .masa = 10, .predkosc = 0.5 };
    MasaPunktowa m2 = { .masa = 2, .predkosc = 0 };
    Sprezyna sprezyna = {
        .stala = 200,
        .dlugosc = 1,
        .m1 = &m1,
        .m2 = &m2
    };

    printf("Masa 1: %lf jak rowniez i %lf", m1.masa, (*sprezyna.m1).masa);
    getchar();
    return 0;
}